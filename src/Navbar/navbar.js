import './navbar.css'
import {Image} from "react-bootstrap";
import Logo from './logoBIG.png'

function Navbar() {
return(
    <div className="navbar">
        
        <div className='items'>
        <Image className="logo" src={Logo}/>
        <h1 className="name">Password Vault</h1>
        </div>
        
    </div>
)
}

export default Navbar;
