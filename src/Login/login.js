
import './login.css';
import React,{useState} from 'react';
import {Card, Form, Button} from "react-bootstrap";


function Login() {
  const [username,setUsername] =useState("")
  const[password,setPassword] = useState("")
  const [jwt,setJwt]= useState ("")
  //const location = useLoction();
  

function sendLoginReq(){
  const requestBody = {
    username: username,
    password: password,
  };
  console.log(username)
  console.log(password)

  fetch("users/login", {
    headers: { 
      "Content-Type" : "application/json",
    },
    method: "POST",
    body: JSON.stringify(requestBody),
  })
  .then((response)=> {
    if (response.status ===200)
    {
      console.log(response)
      return Promise.all([response.json(), response.headers]);}
    
    else
    return Promise.reject("Invalid login attempt");
  })
  .then (([body,headers]) => {
    setJwt(headers.get("authorization"));
    window.location.href ="home";
  })
.catch((message) =>{
  alert(message)
  });
}
  return (
      <div className="login">
        <Card style={{ width: '25rem' ,height:'30rem'}} className="card">
          <Card.Body className="card-body">
            <Card.Title className="card-title">Password Vault</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Login</Card.Subtitle>
            <Card.Text>
              <Form.Control className="form-text" type="text" id ="username" value={username} onChange={(event) => setUsername(event.target.value)} placeholder="Email/Username"   />
              <Form.Control className="form-password" type="password" id ="password" value= {password} onChange={(event) => setPassword(event.target.value)} placeholder="Password" />
            </Card.Text>
            <Button className="button" variant="secondary" type="button"  id ="submit" onClick = {() => sendLoginReq()}>Sign In</Button>
          </Card.Body>
        </Card>
      </div>
  );
}

export default Login;
