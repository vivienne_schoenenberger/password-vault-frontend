import {Modal, Button, Form, Row, Col} from "react-bootstrap";
import "./modal.css"
import {useState} from "react";

function PasswordModal({handleClose}){
    const [nameTag, setNameTag] = useState("");
    const [usage, setUsage] = useState("");
    const [password, setPassword] = useState("");
    const [remarks, setRemarks] = useState("");
    const [jwt,setJwt]= useState ("")
    const [text, setText] = useState("Save");

    function sendNewPassword() {
        const requestBody = {
            nameTag: nameTag,
            usage: usage,
            password: password,
            remarks: remarks
        }

        fetch("/entries",{
            headers: {
                "content-Type" : "application/json",
            },
            method: "POST",
            body: JSON.stringify(requestBody),
        })
            .then((response) => {
                if(response.status === 200)
                {
                    setText("Successfully saved!")
                }

            })
            .then (([body, headers]) => {
                setJwt(headers.get("authorization"));
                window.location.href = "home";
            })
            .catch((message) => {
                alert(message)
            });
    }

    return(

    <Modal show="true" onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title className="title">Add a new Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group as={Row} className="mb-3" controlId="nameTag">
                <Form.Label column sm="3">
                    Name Tag
                </Form.Label>
                <Col sm="9">
                    <Form.Control className="form" type="text" value={nameTag} onChange={(event) => {setNameTag(event.target.value)}} />
                </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3" controlId="usage">
                    <Form.Label column sm="3">
                        Usage
                    </Form.Label>
                    <Col sm="9">
                        <Form.Control className="form" type="text" value={usage} onChange={(event) => {setUsage(event.target.value)}} />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3" controlId="password">
                    <Form.Label column sm="3">
                        Password
                    </Form.Label>
                    <Col sm="9">
                        <Form.Control className="form" type="text" value={password} onChange={(event) => {setPassword(event.target.value)}}/>
                    </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3" controlId="remarks">
                    <Form.Label column sm="3">
                        Remarks
                    </Form.Label>
                    <Col sm="9">
                        <Form.Control className="form" type="text" value={remarks} onChange={(event) => {setRemarks(event.target.value)}}/>
                    </Col>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" className="password-button" onClick={() => sendNewPassword()}>
               Save
            </Button>
        </Modal.Footer>
    </Modal>
    )
}

export default PasswordModal;