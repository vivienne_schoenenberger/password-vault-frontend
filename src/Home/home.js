import Navbar from "../Navbar/navbar"
import PasswordModal from "../Modal/modal"
import './home.css';
import React,{useState} from 'react';
import {Button} from "react-bootstrap";

function App() {
    const [showModal, setShowModal] = useState(false);
    const handleOpen = () => setShowModal(true);
    const handleCloseModal = () => setShowModal(false);

  return (
    <div>
    <div className="App">
      
     <Navbar/>
      <div>

          <Button variant="secondary" onClick={handleOpen} className="modal-button">
              Add Password
          </Button>

          {showModal ? <PasswordModal handleClose={handleCloseModal}></PasswordModal> : <></>}


      <table>
    <thead>
    <tr>
        <th>NAME TAG</th>
        <th>USAGE</th>
        <th>PASSWORD</th>
        <th>REMARKS</th>
    </tr>
    </thead>
    <tbody id="entryDisplay">
    <tr>
        <td colspan="3">Loading...</td>
    </tr>
    </tbody>
</table>
      </div>
    </div>
    </div>
  );
}

export default App;
