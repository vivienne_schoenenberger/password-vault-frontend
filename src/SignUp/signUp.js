import './signUp.css'
import React,{useState} from 'react';
import {Card, Form, Button} from "react-bootstrap";
//import {useLocation, useNavigate} from "react-router"


function Login() {
  const [username,setUsername] =useState("")
  const[password,setPassword] = useState("")
  //const [jwt,setJwt]= useState ("","jwt")
  //const location = useLocation();


function sendLoginReq(){
  const requestBody = {
    username: username,
    password: password,
  };
  console.log(username)
  console.log(password)

  fetch("users/signUp", {
    crossDomain:true,
    headers: { 
      "Content-Type" : "application/json",
    },
    method: "post",
    body: JSON.stringify(requestBody),
  })
  .then((response) =>{
    if (response.status === 200){

      alert("signed in " + username);
      window.location.href ="login"
      
    }
    else
    return alert("Invalid login")
  })

.catch(function(error) {
  console.log('There has been a problem with your fetch operation: ' + error.message);
   // ADD THIS THROW error
    throw error;
  });
}
  return (
      <div className="login">
        <Card style={{ width: '25rem' ,height:'30rem'}} className="card">
          <Card.Body className="card-body">
            <Card.Title className="card-title">Password Vault</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Sign In</Card.Subtitle>
            <Card.Text> 
              <Form.Control className="form-text" type="text" id ="username" value={username} onChange={(event) => setUsername(event.target.value)} placeholder="Email/Username"   />
              <Form.Control className="form-password" type="password" id ="password" value= {password} onChange={(event) => setPassword(event.target.value)} placeholder="Password" />
            </Card.Text>
            <Button className="button" variant="secondary" type="button"  id ="submit" onClick = {() => sendLoginReq()}>Sign In</Button>
            <a className="linkToLogin" href='/login'> already have an account?</a>
          </Card.Body>
        </Card>
      </div>
  );
}

export default Login;
